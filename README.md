## Test: VR-FrontendTest-v1.1

This is a simple React.js + Redux shopping cart app.

To install:

```
yarn

or

npm install
```

*I'm using the latest version of Node 12.6 and npm 6.10.0. I use  nvm to manage Node versions*

To run:

```
yarn start

or

npm start
```

This will run webpack dev server.

*A deprecation warning will appear:*
`[ESLINT_LEGACY_ECMAFEATURES] DeprecationWarning: The 'ecmaFeatures' config file property is deprecated, and has no effect. (found in ".eslintrc » eslint-config-react")`
*This is related to one of the dependencies and doesn't affect the app.*

### To view the app, navigate to:

`http://localhost:3000/dist/index.html`


__To run Flow type checker:__

`yarn flow`

__To run tests:__

`yarn test`
