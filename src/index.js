import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor, store } from './store';
import ProductsContainer from './components/products/products-container';
import CartContainer from './components/cart/cart-container';
import './main.scss';

class App extends React.Component {
  render () {
    return (
      <React.Fragment>
        <ProductsContainer />
        <CartContainer />
      </React.Fragment>
    );
  }
}

const root = document.getElementById("app")

if (root) {
  ReactDOM.render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}><App /></PersistGate>
    </Provider>,
    root
  )
}
