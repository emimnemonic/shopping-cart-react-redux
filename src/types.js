// @flow

export type Product = {
  name: string,
  price: number
}

export type CartItem = {
  product: Product,
  quantity: number
}

export type UpdateCartAction = 'ADD' | 'REMOVE'

export type Action = { type: 'GET_CART' }
                                  | { type: 'GET_PRODUCTS' }
                                  | { type: 'ADD_LINE_ITEM', item: CartItem }
                                  | { type: 'REMOVE_LINE_ITEM', item: Product }
                                  | { type: 'UPDATE_LINE_ITEM', item: Product, updateType: string };
