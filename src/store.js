import { createStore } from 'redux';
import { combineReducers } from 'redux';
import storage from 'redux-persist/lib/storage';
import { persistStore, persistReducer } from 'redux-persist';
import productsReducer from './components/products/products-reducer';
import cartReducer from './components/cart/cart-reducer';

const appReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer
})

const persistConfig = {
  key: 'root',
  storage: storage
};


const pReducer = persistReducer(persistConfig, appReducer);

export const store = createStore(pReducer);
export const persistor = persistStore(store);
