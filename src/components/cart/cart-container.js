import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCart, removeLineItem, updateLineItem } from './cart-actions';
import Cart from './cart-view';

const mapStateToProps = state => ({
  cart: state.cart.cart,
  total: state.cart.total
})

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    getCart,
    removeLineItem,
    updateLineItem
  }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
