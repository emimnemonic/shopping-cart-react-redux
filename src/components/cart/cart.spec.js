import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import 'jest-dom/extend-expect';

import Cart from './cart-view';

let emptyCart = []

let mockCart = [
  {
    product: {
      name: "Axe",
      price: 190.51
    },
    quantity: 1
  },
  {
    product: {
      name: "Chisel",
      price: 13.9
    },
    quantity: 2
  }
]

describe('Cart component', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  afterEach(cleanup)

  it('renders with content', () => {
    const { container } = render(<Cart cart={emptyCart} total={0}/>)
    expect(container).toHaveTextContent('Cart')
    expect(container).toMatchSnapshot()
  })

  it('contains a list of cart products', () => {
    const { container, getByTestId } = render(<Cart cart={mockCart} total={0}/>)
    expect(getByTestId('cart-item-0')).toHaveTextContent('Axe')
    expect(getByTestId('cart-item-1')).toHaveTextContent('2')
    expect(getByTestId('cart-item-1')).toHaveTextContent('Total: $27.80')
    expect(container).toMatchSnapshot()
  })

  it('contains a button that removes item from cart when clicked', () => {
    const removeLineItem = jest.fn()
    const { getByTestId } = render(<Cart
      cart={mockCart}
      removeLineItem={removeLineItem}
      total={0}/>)
    fireEvent.click(getByTestId('remove-item-button-0'))
    expect(removeLineItem).toHaveBeenCalledTimes(1)
  })

  it('contains a button that updates item in cart when clicked', () => {
    const updateLineItem = jest.fn()
    const { getByTestId } = render(<Cart
      cart={mockCart}
      updateLineItem={updateLineItem}
      total={0}/>)
    fireEvent.click(getByTestId('remove-item-button-1'))
    expect(updateLineItem).toHaveBeenCalledTimes(1)
  })
})
