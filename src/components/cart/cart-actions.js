// @flow

import type { Product, CartItem, UpdateCartAction } from '../../types';

export const GET_CART = 'GET_CART';
export const ADD_LINE_ITEM = 'ADD_LINE_ITEM';
export const REMOVE_LINE_ITEM = 'REMOVE_LINE_ITEM';
export const UPDATE_LINE_ITEM = 'UPDATE_LINE_ITEM';

export function getCart () {
  return {
    type: GET_CART
  }
}

export function addLineItem (item: CartItem) {
  return {
    type: ADD_LINE_ITEM,
    item
  }
}

export function removeLineItem (item: Product) {
  return {
    type: REMOVE_LINE_ITEM,
    item
  }
}

export function updateLineItem (item: Product, updateType: UpdateCartAction) {
  return {
    type: UPDATE_LINE_ITEM,
    item,
    updateType
  }
}
