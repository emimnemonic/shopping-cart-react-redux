import cartReducer from './cart-reducer'
import * as actions from './cart-actions'
import * as mocks from './cart-mocks'

describe('Cart reducer', () => {
  it('returns the initial state', () => {
    expect(cartReducer(undefined, {})).toEqual(mocks.mockInitState)
  })

  it('handles adding line item to cart', () => {
    const addItemAction = {
      type: actions.ADD_LINE_ITEM,
      item: mocks.mockItem
    }
    expect(cartReducer(mocks.mockInitState, addItemAction)).toEqual(mocks.mockCartStateAdded)
  })

  it('handles removing line item from cart', () => {
    const removeItemAction = {
      type: actions.REMOVE_LINE_ITEM,
      item: mocks.mockProduct
    }
    expect(cartReducer(mocks.mockCartState, removeItemAction)).toEqual(mocks.mockCartStateRemoved)
  })

  it('handles updating line item by removing item in cart', () => {
    const updateItemAction = {
      type: actions.UPDATE_LINE_ITEM,
      item: mocks.mockProduct2,
      updateType: 'REMOVE'
    }
    expect(cartReducer(mocks.mockCartState, updateItemAction)).toEqual(mocks.mockCartStateUpdatedRemoved)
  })

  it('handles updating line item by adding iten to cart', () => {
    const updateItemAction = {
      type: actions.UPDATE_LINE_ITEM,
      item: mocks.mockProduct,
      updateType: 'ADD'
    }
    expect(cartReducer(mocks.mockCartState, updateItemAction)).toEqual(mocks.mockCartStateUpdatedAdded)
  })
})
