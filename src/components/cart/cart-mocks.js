export const mockCartState = {
  cart: [{
    product: {
      name: "Axe",
      price: 190.51
    },
    quantity: 1
  }, {
    product: {
      name: "Chisel",
      price: 13.90
    },
    quantity: 2
  }],
  total: 218.31
}

export const mockCartStateAdded = {
  cart: [{
    product: {
      name: "Axe",
      price: 190.51
    },
    quantity: 1
  }],
  total: 190.51
}

export const mockCartStateRemoved = {
  cart: [{
    product: {
      name: "Chisel",
      price: 13.90
    },
    quantity: 2
  }],
  total: 27.80
}

export const mockCartStateUpdatedRemoved = {
  cart: [{
    product: {
      name: "Axe",
      price: 190.51
    },
    quantity: 1
  }, {
    product: {
      name: "Chisel",
      price: 13.90
    },
    quantity: 1
  }],
  total: 204.41
}

export const mockCartStateUpdatedAdded = {
  cart: [{
    product: {
      name: "Axe",
      price: 190.51
    },
    quantity: 2
  }, {
    product: {
      name: "Chisel",
      price: 13.90
    },
    quantity: 2
  }],
  total: 408.82
}

export const mockProduct = {
  name: "Axe",
  price: 190.51
}

export const mockProduct2 = {
  name: "Chisel",
  price: 13.90
}

export const mockItem = {
  product: {
    name: "Axe",
    price: 190.51
  },
  quantity: 1
}

export const mockInitState = {
  cart: [],
  total: 0
}
