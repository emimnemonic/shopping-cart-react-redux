// @flow

import {
  GET_CART,
  ADD_LINE_ITEM,
  REMOVE_LINE_ITEM,
  UPDATE_LINE_ITEM
} from './cart-actions';
import type { CartItem, Action } from '../../types';

type CartState = {
  cart: Array<CartItem>,
  total: number
}

const initState = {
  cart: [],
  total: 0
}

export default (state: CartState = initState, action: Action): CartState => {
  switch (action.type) {
  case GET_CART:
    return {
      ...state,
      cart: state.cart
    }

  case ADD_LINE_ITEM:
    return {
      ...state,
      cart: [
        ...state.cart,
        action.item
      ],
      total: parseFloat((state.total + action.item.product.price).toFixed(2))
    }

  case REMOVE_LINE_ITEM:
    return {
      ...state,
      cart: state.cart.filter((item: CartItem) => item.product.name !== action.item.name),
      total: parseFloat((state.total - action.item.price).toFixed(2))
    }

  case UPDATE_LINE_ITEM:
    const ADD = 'ADD'
    return {
      ...state,
      cart: state.cart.map((item: CartItem) => {
        if (item.product.name === action.item.name) {
          return {
            ...item,
            quantity: action.updateType === ADD ? item.quantity + 1 : item.quantity - 1
          }
        }
        return item
      }),
      total: action.updateType === ADD ? parseFloat((state.total + action.item.price).toFixed(2)) :
        parseFloat((state.total - action.item.price).toFixed(2))
    }

  default:
    return state
  }
}
