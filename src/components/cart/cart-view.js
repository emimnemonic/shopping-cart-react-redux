// @flow
import React from 'react';
import type { Product, CartItem, Action, UpdateCartAction } from '../../types';

type Props = {
  cart: Array<CartItem>,
  total: number,
  removeLineItem: (Product) => Action,
  updateLineItem: (Product, UpdateCartAction) => Action
}

function Cart (props: Props) {
  const { cart, total, removeLineItem, updateLineItem } = props
  const handleRemoveItem = (product: Product) => {
    if (cart.length > 0) {
      const existing = cart.filter((item) => item.product.name === product.name)
      if (existing.length > 0 && existing[0].quantity > 1) {
        return updateLineItem(product, 'REMOVE')
      }
    }
    return removeLineItem(product)
  }

  if (cart.length > 0) {
    return (
      <React.Fragment>
        <ul className="vertical-list cart-list" data-testid="cart-list">
          <h2>Cart</h2>
          { cart.map((item: CartItem, index: number) => (
            <li
              key={index}
              className="list-item cart-item"
              data-testid={"cart-item-" + index.toString()}>
              <span>{ item.product.name } - ${ item.product.price.toFixed(2) } - { item.quantity }x<br />
              Total: ${ (item.quantity * item.product.price).toFixed(2) }</span>
              <button
                className="button button-action button-remove"
                onClick={() => handleRemoveItem(item.product)}
                data-testid={"remove-item-button-" + index.toString()}>Remove item</button>
            </li>
          )) }
        </ul>
        <span className="total" data-testid="cart-total">Total: ${ total.toFixed(2) }</span>
      </React.Fragment>
    )
  } else {
    return <div className="empty-cart"> <h2>Cart</h2>Your cart is empty</div>
  }
}

export default Cart
