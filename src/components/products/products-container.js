import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getProducts } from './products-actions';
import { addLineItem, updateLineItem } from '../cart/cart-actions';
import Products from './products-view';

const mapStateToProps = state => ({
  products: state.products.products,
  cart: state.cart.cart
})

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    getProducts,
    addLineItem,
    updateLineItem
  }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(Products)
