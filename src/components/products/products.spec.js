import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import 'jest-dom/extend-expect'
import data from '../../data';

import Products from './products-view';

describe('Products component', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  afterEach(cleanup)

  // sanity check
  it('renders with content', () => {
    const { container } = render(<Products products={data}/>)
    expect(container).toHaveTextContent('Products')
    expect(container).toMatchSnapshot()
  })

  it('contains a list of products', () => {
    const { getByTestId } = render(<Products products={data}/>)
    expect(getByTestId('products-list')).toHaveClass('vertical-list')
    expect(getByTestId('products-list').children.length).toBe(6)
    expect(getByTestId('product-3')).toHaveTextContent('Chisel')
    expect(getByTestId('product-0')).toHaveTextContent('125.76')
  })

  it('contains a button that adds item to cart when clicked', () => {
    const addLineItem = jest.fn()
    let mockCart = []
    const { getByTestId } = render(<Products
      products={data}
      addLineItem={addLineItem}
      cart={mockCart}/>)
    fireEvent.click(getByTestId('add-product-button-2'))
    expect(addLineItem).toHaveBeenCalledTimes(1)
  })

  it('contains a button that updates item to cart when clicked', () => {
    const updateLineItem = jest.fn()
    let mockCart = [
      {
        product: {
          name: "Bandsaw",
          price: 562.14
        },
        quantity: 1
      }
    ]
    const { getByTestId } = render(<Products
      products={data}
      updateLineItem={updateLineItem}
      cart={mockCart}/>)
    fireEvent.click(getByTestId('add-product-button-2'))
    expect(updateLineItem).toHaveBeenCalledTimes(1)
  })
})
