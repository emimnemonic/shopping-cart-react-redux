// @flow

import data from '../../data';

import {
  GET_PRODUCTS
} from './products-actions';
import type { Product, Action } from '../../types';

type ProductState = {
  products: Array<Product>
}

const initState = {
  products: data
}

export default (state: ProductState = initState, action: Action): ProductState => {
  switch (action.type) {
  case GET_PRODUCTS:
    return {
      ...state
    }

  default:
    return state
  }
}
