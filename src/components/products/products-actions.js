// @flow

export const GET_PRODUCTS = 'GET_PRODUCTS';

export function getProducts () {
  return {
    type: GET_PRODUCTS
  }
}
