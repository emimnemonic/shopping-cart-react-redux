// @flow
import React from 'react';
import type { Product, CartItem, Action, UpdateCartAction } from '../../types';

type Props = {
  products: Array<Product>,
  cart: Array<CartItem>,
  addLineItem: (CartItem) => Action,
  updateLineItem: (Product, UpdateCartAction) => Action
}

function Products (props: Props) {
  const { products, cart, addLineItem, updateLineItem } = props

  const handleAddProduct = (product: Product) => {
    if (cart.length > 0) {
      const existing = cart.filter((item) => item.product.name === product.name)
      if (existing.length > 0) {
        return updateLineItem(product, "ADD")
      }
    }

    const lineItem = {
      product,
      quantity: 1
    }
    return addLineItem(lineItem)
  }

  return (
    <ul className="vertical-list products-list" data-testid="products-list">
      <h2>Products</h2>
      { products.map((product: Product, index: number) => (
        <li key={index} className="list-item products-item" data-testid={"product-" + index.toString()}>
          <span>{ product.name } - ${ product.price.toFixed(2) }</span>
          <button
            data-testid={"add-product-button-" + index.toString()}
            className="button button-action button-add"
            onClick={() => handleAddProduct(product)}>Add item</button>
        </li>
      ))}
    </ul>
  )
}

export default Products
